# Pour créer un site web et ressources d'une UE avec Hugo

[La page de l'UE est ici](http://alexandre.meyer.pages.univ-lyon1.fr/hugo-web-minimal/)




## Explication de la génération

Le site web est désormais fabriqué par ```Hugo``` (thème [congo](https://jpanther.github.io/congo/)). les sources se trouvent dans le répertoire ```web```. Le site web est mis à jour par intégration continue (CI/CD) à chaque fois que vous faites un push (rien besoin d'autre, à part attendre quelques secondes). Le script d'intégration continue est ```.gitlab-ci.yml```.  Pour voir le résultat du script de génération, depuis l'interface allez dans Buil/CI/Jobs.

Le fichier ```site/config.toml``` permet de configurer la génération du site. Mais noramlement il n'y a pas besoin d'y toucher.
   * Les pages web sont générées à partir du répertoire ```web/content```. 
   * La page principale du site est ```web/content/_index.html```. Il faut bien laissé le ```_```, il indique qu'il y a des sous-répertoires 
   * ```web/static``` : les fichiers autres (pdf, images, sujets, etc.) sont à ranger dedans. Par exemple, il y a 
      * ```web/static/images``` pour les images du site;
      * ```web/static/doc``` documents généraux de l'UE;
   * Pour changer l'icone de la page web, il faut générer des icones avec un [flavicon générator](https://favicon.io/favicon-generator/), copier toutes les images dans le repertoire `static` et ajouter dans `config.toml` une ligne dans `param` avec   `favicon = "favicon.ico"`.


## Si Fork
Par défaut, gitlab ajoute des '-' un peu partout, vous pouvez changer l'adresse web dans Settings/General/Advanced/Change path.

## Si problème de thème non utilisé
Dans deploy/page il ne faut pas cocher  "Use unique domain"


## Tester le site en local
Pour tester vos mises à jour en local :
   * installer hugo : ```sudo apt install hugo```
   * dans le répertoire web, faire ```hugo serve```
   * dans votre navigateur, entrez l'url ```localhost:8000```

Vous pouvez aussi essayer de contruire le site en static en faisant juste ```hugo``` : le site sera constuit dans le répertoire public.


## Des outils 

* Pour convertir du DOKUWIKI en Markdown, on peut utiliser pandoc (ce n'est pas parfait)
  * [Pandoc online](https://pandoc.org/try/)

* Changer taille d'une image `<img src="doc/chara.jpg" width="400" class="center">`
* Un pargraphe justifié `<p style="text-align:justify;">` blahblah `</p>`
