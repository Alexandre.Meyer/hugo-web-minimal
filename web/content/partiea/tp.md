  

# TP d'Animation de personnage (M1)

  
  

## TP partie 1 : affichage

![](/charanim_interpolation.png)  
Vous allez créer un module Skeleton.h/.cpp (ce code n'est qu'indicatif, vous êtes libre de vos structures de données). Cette classe va stocker un tableau de toutes les articulations (SkeletonJoint) du squelette et pour chaque articulation stocke l'identifiant de l'articulation parent et la matrice de passage de l'articulation vers le monde.

Le fichier est déjà présent dans le code départ avec des TODO à compléter :

``` 

class Skeleton
{
public:

   struct SkeletonJoint
   {
      int m_parentId;    // Le numéro du père dans le tableau de CAJoint de CASkeleton
      Transform m_l2w;  // La matrice passant du repère de l'articulation vers le monde
   };

   Skeleton() {}

   //! Créer un squelette ayant la même structure que définit dans le BVH c'est à dire
   //! creer le tableau de SkeletonJoint à la bonne taille, avec les parentId initialsé pour chaque case
   void init(const BVH& bvh);

   //! Renvoie la position de l'articulation i en multipliant le m_l2w par le Point(0,0,0)
   Point getJointPosition(int i) const;

   //! Renvoie l'identifiant de l'articulation père de l'articulation numéro i
   int getParentId(const int i) const;
   
   //! Renvoie le nombre d'articulation
   int numberOfJoint() const;

   //! Positionne ce squelette dans la position n du BVH. 
   //! Assez proche de la fonction récursive (question 1), mais range la matrice (Transform)
   //! dans la case du tableau. Pour obtenir la matrice allant de l'articulation local vers le monde,
   //! il faut multiplier la matrice allant de l'articulation vers son père à la matrice du père allant de
   //! l'articulation du père vers le monde.
   void setPose(const BVH& bvh, int frameNumber);


protected:
   //! L'ensemble des articulations.
   //! Remarque : la notion de hiérarchie (arbre) n'est plus nécessaire ici,
   //! pour tracer les os on utilise l'information "parentID" de la class CAJoint
   std::vector<SkeletonJoint> m_joint;
};

```

  
Dans le Viewer vous devez :
 * Déclarer un Skeleton en variable de la classe
 * écrire une fonction qui fait l'affichage

``` 
    void CharAnimViewer::skeletonDraw(const Skeleton& ske);
```

 * initaliser l'instance de Skeleton dans la fonction init
 * Appeler setPose dans la fonction update

  
Remarques :
 * On sépare bien l'affichage de la gestion du squelette pour pouvoir réutiliser le code Skeleton avec une autre librairie d'affichage.
 * On ne s'occupe pas du temps pour l'instant mais uniquement du numéro de la posture.\\
 * Vous pouvez trouvez des BVH dans le répertoire data du code de départ. Notamment le fichier robot.bvh pour debuguer.

  
  
## TP partie 2 : Contrôleur d'animation

##### Déplacer une sphère au clavier

Ecrivez une class CharacterControler qui à partir des touches claviers contrôlera le déplacement d'un personnage. Dans une 1er temps faites juste déplacer une boule : accélérer, freiner, tourner à droite, tourner à gauche, sauter. Ce contrôleur comportera une position et une vitesse. La vitesse sera modifiée par les flèches (ou un pad) et la position sera mise à jour dans la fonction update du Viewer en utilisant le paramètre "delta" recu par la fonction update.

Une classe de Controller peut ressembler à ceci.

``` 
    class CharacterController
    {
    public:
        CharacterController() : ... {}

        void update(const float dt);

        void turnXZ(const float& rot_angle_v);
        void accelerate(const float& speed_inc);
        void setVelocityMax(const float vmax);

        const Point position() const;
        const Vector direction() const;
        float velocity() const;
                const Transform& controller2world() const { return m_ch2w; }

    protected:
                Transform m_ch2w;   // matrice du character vers le monde
                                    // le personnage se déplace vers X
                                    // il tourne autour de Y
                                    // Z est sa direction droite
                                    
        float m_v;          // le vecteur vitesse est m_v * m_ch2w * Vector(1,0,0)
        float m_vMax;       // ne peut pas accélérer plus que m_vMax
    };
```

  

##### Déplacer un personnage au clavier

Dans un 2e temps, votre contrôleur comportera également une série
d'animation bvh : attendre, marcher, courir, et donner un coup de pied.
En fonction de l'action que veut faire le joueur appuyant sur des
touches vous changerez d'animation. Vous coderez la machine à états
finis (FiniteStateMachine) de l'image ci-dessous. Les cercles sont les
états (l'animation en train d'être jouée), les rectangles rouges sont
les éventements et les carrés bleus sont les actions à effectuer
(fonction de la classe). Ce changement se fera brutalement. Ne vous
occupez pas non plus des pieds qui glissent sur le sol. Un meilleur
contrôle peut-être fait la construction d'un graphe d'animation.

![](/fsm.png)

  
  

## TP partie 3 : Transition et plus

a) Pour améliorer le réalisme, il serait bon de faire les transitions
entre deux animations en choisissant deux poses des animations qui sont
proches. Pour cela il faut calculer la distance entre deux poses
d'animations (Voir les infos dans le sujet Graphe d'animation).  
  

b) Pour aller encore plus loin, on peut construire un automate de
manière complètement automatique, on appelle alors ceci un graphe
d'animation. [Voir le sujets de TP
suivants.](https://perso.liris.cnrs.fr/alexandre.meyer/public_html/www/doku.php?id=master_charanim_tp_m2_cpp&s[]=graphe#tp_3e_partiegraphe_d_animation)  
  

a.bis) Indépendamment de la machine à état ou du graphe, si vous voulez
gérer le temps de manière plus juste, il faudrait récupérer le temps
réellement écoulé depuis l'affichage précédent. Ceci vous fera ne vous
fera pas tomber précisément sur une frame stocké dans le clip (BVH). Il
faudra donc interpoler entre les 2 frames. Le résultat sera de l'ordre
du détail lors de l'affichage mais si vous voulez que votre moteur
d'animation tourne sur toutes les machines indépendamment du CPU, il
faut le faire. Cette interpolation peut également servir pour passer
d'un clip à un autre.

  
  
  

## TP partie 3.PLUS : graphe d'animation

*  -* [Motion Graph de l'article original](http://www.cs.wisc.edu/graphics/Gallery/kovar.vol/MoGraphs/);
* Des BVH avec squelette compatible pour le graphe sont donné dans le git, répertoire Second\_Life.  

Nous avons remarqué dans la partie 1 que la transition d'animation ne fonctionne bien que lorsque les deux poses du squelette sont assez proches (il faut bien sûr également que les deux squelettes aient la même topologie). L'idée d'un graphe d'animation est de construire un graphe où chaque noeud correspond à une pose d'une animation et où chaque arrête définit qu'une transition est possible entre les deux poses.

  

#### Comparaison de deux poses d'animation

Pour construire un graphe d'animation à partir d'une ou plusieurs animations, on doit être capable de comparer deux poses d'animation. Une distance de 0 indique que les deux poses sont identiques. Une distance grande indique que les 2 poses sont très différentes.

A partir de la classe Skeleton, écrivez la fonction de calcul de distance entre deux poses de squelette. Cette fonction est déjà présente dans la classe Skeleton plus haut mais en commentaire. Cette fonction calcule itérativement sur toutes les articulations la somme des distances euclidienne entre chaque articulation de deux squelettes aillant la même topologie mais dans des poses différentes.

```
    friend float Skeleton::Distance(const Skeleton& a, const Skeleton& b);
```

  
Remarque : il est important de ne pas tenir compte de la translation et de la rotation de l'articulation racine. Une même pose a deux endroits du monde doit donner une distance de 0. Dans un 1er temps, votre personnage aura son noeud root centré en (0,0,0), puis dans la dernière partie de cette question, vous traiterez le centre de gravité.

  
  

#### Construction du graphe

Ecrivez un module MotionGraph qui contiendra un ensemble de BVH et le graphe d'animation définissant des transitions dans cette ensemble d'animation.
 * Un noeud du graphe=(Identifiant d'une animation + un numéro de pose); 
 * un arc du graphe entre deux poses indique la transition possible entre ces deux poses. Deux poses sont compatibles à la transition quand la distance entre les deux squelettes sont inférieurs à un certain seuil fixé empiriquement.

  
Vous pouvez créer un module CACore/CAMotionGraph.h/.cpp

```
    class MotionGraph
    {
       ...
    protected:
       //! L'ensemble des BVH du graphe d'animation
       std::vector<BVH> m_BVH;
    
       //! Un noeud du graphe d'animation est repéré par un entier=un identifiant
       typedef int GrapheNodeID;
    
       //! Une animation BVH est repérée par un identifiant=un entier 
       typedef int BVH_ID;
       
       //! Un noeud du graphe contient l'identifiant de l'animation, le numéro 
       //! de la frame et les identifiants des noeuds successeurs 
       //! Remarque : du code plus "joli" aurait créer une classe CAGrapheNode
       struct GrapheNode
       {
         BVH_ID id_bvh;
         int frame;
         std::vector<GrapheNodeID> ids_next;     //! Liste des nœuds successeurs 
       };
    
    
       //! Tous les noeuds du graphe d'animation
       std::vector<GrapheNode> m_GrapheNode;
    
    };
```
  
  

#### Navigation dans le graphe

Une fois ce graphe construit, on peut définir différente manière de naviguer dedans :
   * Un parcours aléatoire dans le graphe (juste pour vérifier que le graphe est ok);
   * L'utilisateur donne des directions au clavier => le parcours dans le graphe est conditionné par ces contraintes.


#### Gestion correcte du centre de gravité

Pour chaque arc du graphe, vous devez stocker la transformation (soit une matrice 4x4, soit un quaternion et une translation) du noeud root (souvent le centre de gravité) entre la pose i et la pose i+1. Cette transformation sera appliqué au noeud root de votre personnage quand il empruntera l'arc.

  
  
  

## NON DEMANDE CETTE ANNEE : Animation physique (voir la partie de F. Zara à la place)

#### Particules

![](/charanim_ball.png)

Dans la fonction init de la class CharAnimViewer indiquez un nombre de particules non nul :

``` 
    m_world.setParticlesCount( 10 );
```

Dans la fonction render, il faut afficher les particules en dé-commentant cette ligne :

``` 
    m_world.draw();
```

Vous verrez alors les particules s'afficher, mais elles ne seront pas animées. Pour calculer la physique sur les particules, il y a deux
classes **PhysicalWorld** et **Particle**. Regardez le fichier Particles.h. Il faudra compléter les fonctions update, collision et groundCollision :

``` 
    void update(const float dt = 0.1f)
    void groundCollision()
    void collision(const Point& p, const float radius)
```

Le code de update doit mettre à jour la vitesse avec l'équation F=m.a où a = dv/dt  
Et mettre à jour la position avec l'équation habituelle p = p + v.t  

[Regardez les explications dans la vidéo de cours ou ici](https://perso.liris.cnrs.fr/alexandre.meyer/teaching/master_charanim/aPDF_COURS_M1/M1_TP_PhysicsAnimation_ParticulesMassesRessorts.pdf).

  

#### Interaction personnage/particules

Pour ajouter l'interaction entre votre personnage et des boules/sphères se trouvant dans l'environnement, il faut appeler PhysicalWorld::collision depuis CharAnimViewer::update en parcourant toutes les articulations du personnage. Dans un 1er temps, vous pouvez juste faire disparaitre les particules touchées en faisant passer le rayon de la particule à -1 et faire en sorte que les particules de rayon négatif ne soient pas affichées. Puis ajoutez dans Particle::collision du code pour déplacer les particules en collisions (résoudre les collisions) et changer leur vecteur vitesse.

#### Tissus

Un tissu est composé d'un maillage de Masses/Ressorts. Ajoutez une classe Spring qui va comporter :
 * la raideur du ressort
 * la longueur au repos du ressort
 * idA et idB : l'identifiant des 2 particules aux extrémités du ressort. Ces 2 identifiants sont l'indices de deux particules dans le tableau de particules stocké dans la classe PhysicalWorld.

Munissez cette classe d'une fonction *addForce* qui calcule les forces qu'applique le ressort sur les 2 particules.
```
    void Spring::addForce(vector<Particles>& part, const float dt)
```


Dans la classe PhysicalWorld, ajoutez un tableau de ressort :

``` 
   std::vector<Spring> m_springs;
```

Le constructeur de PhysicalWorld créera un tissu avec des masses ressorts sous la forme de l'image ci-dessous. La génération se fera procéduralement.

![](/mass-spring.jpg)


#### Interaction avec le personnage

Normalement, le personnage va pouvoir interagir avec le tissu en passant dessous ...
