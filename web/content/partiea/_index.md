
---
title: ""
description: "Partie Animation de personnage"
---



# Partie Animation de personnage
  * Alexandre Meyer
  * 4h30 CM, 6h30 TP
  * [L'ancienne page de cette partie](https://perso.liris.cnrs.fr/alexandre.meyer/public_html/www/doku.php?id=charanim_m1#master_1_informatique_-_ue_m1if37_animation_en_synthese_d_image)

![](../doc_charanim/charanim_tpose.jpg)




## Cours
  * [Systèmes articulés : cinématique directe](https://perso.liris.cnrs.fr/alexandre.meyer/teaching/master_charanim/aPDF_COURS_M1/M1_1_SkeletonBasedAnimation.pdf)
  * [Edition d'animations, Graphe d'animations](https://perso.liris.cnrs.fr/alexandre.meyer/teaching/master_charanim/aPDF_COURS_M1/M1_2_MotionControlAndEditing.pdf)
  * [Capture de mouvements](https://perso.liris.cnrs.fr/alexandre.meyer/teaching/master_charanim/aPDF_COURS_M1/M1_3_MotionCapture.pdf)


## Les vidéos des 3 CM (2020)
[Les vidéos](video)



## Le TP
  * [TP animation de personnage virtuel](tp)
  * [[master_charanim_code|Le code initial]]

#### Rendu
Les archives sont à rendre sur TOMUSS
  * TP Animation de personnage virtuel
    * vous devez rendre une archive avec un readme.txt ou readme.md expliquant ce que vous avez fait et comment lancer le programme
    * une VIDEO de DEMO ou une démo en LIVE
  * Barème "Personnage" 
    * (5 points) TP 1ère partie : affichage d'une animation BVH
    * (3 points) Contrôleur d'animation :  la base (pilotage au clavier d'un déplacement)
    * Machine à état (4 points maximum)
      * basique : pour une machine à état de base avec 4 animations : iddle, marcher, courir, sauter ou kick
      * avancé : une machine à état plus complète avec de nombreuses animations (une dizaine) : voir celles du répertoire motionGraph_second_life, etc.
    * Motion Graphe (6 points max)
      * basique : transition vers un autre animation en cherchant à la volée une transition compatible
      * avancé : un motion graph construit automatiquement en pré-calcul (voir la fin de l'énoncé qui pointe vers un complément de sujet).
    * (3 points) Interpolation entre 2 frames, à utilise pour les transitions ou pour une bonne gestion du temps
    * (1 points) Collision entre personnage et sphères (voir le code PhysicalWorld)
    * (1 points) bonus de qualité/cohérence de la scène
    * Le total fait plus que 20 car les points FSM/graphe d'animation sont à comprendre avec un OU : la machine à état rapporte moins de points, car bien moins difficile.
